About 
-----

`hyperion-bootstrap` is a haskell library for numerical conformal
bootstrap computations. It uses the
[hyperion](https://github.com/davidsd/hyperion/) cluster management
library.

Documentation
-------------

Documentation is here: [https://davidsd.gitlab.io/hyperion-bootstrap/](https://davidsd.gitlab.io/hyperion-bootstrap/)
