{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PolyKinds        #-}

module Hyperion.Bootstrap.TiptopSearch where

import           Control.Monad.Catch                 (MonadCatch)
import           Control.Monad.IO.Class              (MonadIO)
import           Control.Monad.Reader                (MonadReader)
import qualified Data.Map                            as Map
import           Data.Ratio                          (approxRational)
import           Data.Time.Clock                     (UTCTime)
import qualified Data.Vector                         as V
import qualified Bootstrap.Math.AffineTransform  as AT
import qualified Hyperion.Bootstrap.PointCloudSearch as PC
import qualified Hyperion.Bootstrap.Tiptop           as Tiptop
import           Hyperion.Concurrent                 (Concurrently)
import qualified Hyperion.Database                   as DB
import           Linear.V                            (V)
import           Bootstrap.Math.Linear.Literal            (fromV, toV)

data TiptopSearchConfig = TiptopSearchConfig
  { heightResolution :: Int    -- ^ Map the height [-1,1] onto the the
                               -- integer range [0,heightResolution]
  , widthResolution  :: Double -- ^ Use widthResolution when
                               -- converting from Double's back to
                               -- Rationals for the non-height
                               -- coordinates.
  , jumpResolution   :: Double -- ^ tiptop's resolution parameter that
                               -- determines how eagerly it jumps to a
                               -- new level. (TODO: better name?)
  , tiptopExecutable :: FilePath
  , nSteps           :: Int    -- ^ Run the search for this many steps
  , nThreads         :: Int    -- ^ Use this many threads in parallel
  , terminateTime    :: Maybe UTCTime -- ^ Don't start any more
                                      -- computations after this time.
  }

-- | tiptop requires that the height h be an integer. For the
-- application to PointCloudSearch, h is always in the range
-- [-1,1]. Here, we map this range onto the integer range [0,maxGap]
toHeightInt :: Int -> Rational -> Int
toHeightInt maxGap h = round ((h+1)/2 * fromIntegral maxGap)

-- | Should satisfy the identity @toHeightInt . fromHeightInt = id@
fromHeightInt :: Int -> Int -> Rational
fromHeightInt maxGap hInt = 2 * fromIntegral hInt / fromIntegral maxGap - 1

toTTV :: TiptopSearchConfig -> V 4 Rational -> Tiptop.TTV 3 Double
toTTV cfg v = case fromV v of
  (a,b,c,d) -> Tiptop.TTV
    (fmap fromRational $ toV (a,b,c))
    (toHeightInt (heightResolution cfg) d)

fromTTV :: TiptopSearchConfig -> Tiptop.TTV 3 Double -> V 4 Rational
fromTTV cfg (Tiptop.TTV xs hInt) =
  case fromV (flip approxRational (widthResolution cfg) <$> xs) of
    (a,b,c) -> toV (a,b,c, fromHeightInt (heightResolution cfg) hInt)

toTiptopInput :: TiptopSearchConfig -> Map.Map (V 4 Rational) (Maybe Bool) -> Tiptop.Input 3
toTiptopInput cfg pointMap = Tiptop.Input
  { Tiptop.feasible    = filterMap (Just True)
  , Tiptop.infeasible  = filterMap (Just False)
  , Tiptop.in_progress = filterMap Nothing
  , Tiptop.max_gap     = heightResolution cfg
  , Tiptop.resolution  = jumpResolution cfg
  }
  where
    filterMap v =
      fmap (toTTV cfg) $
      V.fromList $
      Map.keys (Map.filter (== v) pointMap)

-- | Assumes all points are inside the hypercube [-1,1]^4. The fourth
-- coordinate is treated as the height, and rasterized with resolution
-- 'heightResolution'.
getNextPoint
  :: TiptopSearchConfig
  -> Map.Map (V 4 Rational) (Maybe Bool)
  -> IO (Maybe (V 4 Rational))
getNextPoint cfg pointMap = do
  result <- Tiptop.tiptop (tiptopExecutable cfg) (toTiptopInput cfg pointMap)
  return $ case result of
    Tiptop.NextPoint v -> Just (fromTTV cfg v)
    Tiptop.NoPoint     -> Nothing

tiptopSearchRegionPersistent
  :: ( MonadIO m, MonadReader env m, DB.HasDB env, MonadCatch m
     , Applicative (Concurrently m)
     )
  => DB.KeyValMap (V 4 Rational) (Maybe Bool)
  -> TiptopSearchConfig
  -> AT.AffineTransform 4 Rational
  -> Map.Map (V 4 Rational) (Maybe Bool)
  -> (V 4 Rational -> m Bool)
  -> m PC.PointCloudStatus
tiptopSearchRegionPersistent results tiptopCfg affine initialPts f =
  PC.pointCloudSearchRegionPersistent results pointCloudCfg affine initialPts f
  where
    pointCloudCfg = PC.PointCloudConfig
      { PC.nThreads        = nThreads tiptopCfg
      , PC.nSteps          = const (nSteps tiptopCfg)
      , PC.getNextPoint    = getNextPoint tiptopCfg
      , PC.addBoundingCube = False
      , PC.terminateTime   = terminateTime tiptopCfg
      }
