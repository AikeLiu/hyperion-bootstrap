{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Hyperion.Bootstrap.SegmentSearch where

import           Control.Applicative
import           Control.Concurrent.Memoize (memoize)
import           Control.Concurrent.STM     (atomically, modifyTVar, newTVarIO,
                                             readTVar)
import           Control.Monad
import           Control.Monad.Catch        (MonadCatch)
import           Control.Monad.IO.Class     (MonadIO, liftIO)
import           Control.Monad.Reader       (MonadReader)
import           Data.Aeson
import           Data.Foldable
import           Data.Maybe
import qualified Data.Set                   as Set
import           Data.Traversable
import           Data.Tree
import           GHC.Generics
import           Hyperion.Concurrent        (Concurrently, mapConcurrently)
import qualified Hyperion.Database          as DB
import           Linear.Matrix
import           Linear.V2
import           Linear.V3
import           Linear.Vector
import           Prelude                    hiding (all, concat, concatMap)

data Segment p = Segment { segmentDepth  :: Int
                         , segmentPoint1 :: p
                         , segmentPoint2 :: p
                         } deriving (Show, Functor, Generic, Eq, Ord, ToJSON)

type SegmentSearch m p = (Segment p -> m Bool) -> m [Segment p]

type LatticeSize v = v Int

data Region v a = Region { regionOrigin  :: v a
                           -- rows are basis vectors
                         , regionBasis   :: v (v a)
                         , regionDensity :: LatticeSize v
                         } deriving (Functor)

rectRegion :: (Num (v a), Num a, Traversable v) => v a -> v a -> v Int -> Region v a
rectRegion p q = Region p (scaled (q-p))

rectRegion2d :: Num a => a -> a -> a -> a -> Int -> Int -> Region V2 a
rectRegion2d x1 x2 y1 y2 nx ny = rectRegion (V2 x1 y1) (V2 x2 y2) (V2 nx ny)

refineSearch :: (Monad m, Applicative (Concurrently m)) => (a -> [a]) -> (a -> m Bool) -> a -> m (Maybe (Tree a))
refineSearch refine f = go
  where
    go a = f a >>= \case
      True  -> fmap (Just . Node a . catMaybes) (mapConcurrently go (refine a))
      False -> pure Nothing

refineSearchList :: (Monad m, Applicative (Concurrently m)) => (a -> [a]) -> (a -> m Bool) -> [a] -> m [a]
refineSearchList refine f as = fmap (concatMap flatten . catMaybes)
                                    (mapConcurrently (refineSearch refine f) as)

bisectRefine :: (Num (v a), Fractional a, Functor v) => Segment (v a) -> [Segment (v a)]
bisectRefine (Segment n p q) = [Segment (n+1) p mid, Segment (n+1) mid q]
  where
    mid = (p + q) ^/ 2

hexRefine :: Fractional a => Segment (V2 a) -> [Segment (V2 a)]
hexRefine (Segment n p q) =
  map seg [(p,u), (p,l), (u,q), (l,q), (l,u)]
  where
    m1 = V2 (V2 (1/3) (-1/3)) (V2 (1/3) (2/3))
    m2 = V2 (V2 (2/3) (1/3))  (V2 (-1/3) (1/3))
    u  = m1 !* p + m2 !* q
    l  = m2 !* p + m1 !* q
    seg (a,b) = Segment (n+1) a b

quadRefine :: Fractional a => Segment (V2 a) -> [Segment (V2 a)]
quadRefine (Segment n p q) =
  map seg [(p,u1), (p,u2), (u1,q), (u2,q)]
  where
    mid = (p + q) ^/ 2
    v   = (q - p) ^/ 2
    u1  = mid + perp v
    u2  = mid - perp v
    seg (a,b) = Segment (n+1) a b

cubicToFccRefine :: Fractional a => Segment (V3 a) -> [Segment (V3 a)]
cubicToFccRefine (Segment n p q) =
  map seg [ (p,u1),  (p,u2),  (p,u3),  (p,u4)
          , (u1,q),  (u2,q),  (u3,q),  (u4,q)
          , (u1,u2), (u2,u3), (u3,u4), (u4,u1)
          ]
  where
    m   = V3 (V3 0 0 1) (V3 1 0 0) (V3 0 1 0)
    mid = (p + q) ^/ 2
    v   = (q - p) ^/ 2
    u1  = mid + m !* v
    u2  = mid + m !* (m !* v)
    u3  = mid - m !* v
    u4  = mid - m !* (m !* v)
    seg (a,b) = Segment (n+1) a b

fccToCubicRefine :: (Fractional a, Eq a) => Segment (V3 a) -> [Segment (V3 a)]
fccToCubicRefine (Segment n p q) = do
  u <- toList (scaled (q-p))
  guard (u /= zero)
  [Segment (n+1) p (p+u), Segment (n+1) (p+u) q]

latticeSearch :: (Monad m, Foldable v, Functor v, Ord a, Num a, Num (v a), Applicative (Concurrently m))
              => (LatticeSize v -> [Segment (v a)])
              -> (Segment (v a) -> [Segment (v a)])
              -> LatticeSize v
              -> SegmentSearch m (v a)
latticeSearch initialSegments refineSegment latticeSize f =
  refineSearchList refine f (initial latticeSize)
  where
    s = fmap fromIntegral latticeSize
    inLattice (Segment _ p q) = all (>= 0) p && all (>= 0) (s-p) &&
                                all (>= 0) q && all (>= 0) (s-q)
    initial = filter inLattice . initialSegments
    refine  = filter inLattice . refineSegment

hex2dLatticeSearch :: (Monad m, Applicative (Concurrently m))
                   => Int
                   -> Int
                   -> LatticeSize V2
                   -> SegmentSearch m (V2 Rational)
hex2dLatticeSearch hexs bisects = latticeSearch initial refine
  where
    V2 e1 e2 = identity
    initial (V2 nx ny) = do
      let lattice = liftA2 V2 [0 .. nx] [0 .. ny]
      p <- map (fmap fromIntegral) lattice
      (a,b) <- [(p,p+e1), (p,p+e2), (p+e1,p+e2)]
      pure (Segment 0 a b)
    refine s@(Segment n _ _) | n < hexs           = hexRefine s
                             | n < hexs + bisects = bisectRefine s
                             | otherwise          = []

quad2dLatticeSearch :: (Monad m, Applicative (Concurrently m))
                    => Int
                    -> Int
                    -> LatticeSize V2
                    -> SegmentSearch m (V2 Rational)
quad2dLatticeSearch quads bisects = latticeSearch initial refine
  where
    initial (V2 nx ny) = do
      let lattice = liftA2 V2 [0 .. nx] [0 .. ny]
      p <- map (fmap fromIntegral) lattice
      e <- basis
      pure (Segment 0 p (p+e))
    refine s@(Segment n _ _) | n < quads           = quadRefine s
                             | n < quads + bisects = bisectRefine s
                             | otherwise           = []

cubic3dLatticeSearch :: (Monad m, Applicative (Concurrently m))
                     => Int
                     -> Int
                     -> LatticeSize V3
                     -> SegmentSearch m (V3 Rational)
cubic3dLatticeSearch cubics bisects = latticeSearch initial refine
  where
    initial (V3 nx ny nz) = do
      let lattice = liftA3 V3 [0 .. nx] [0 .. ny] [0 .. nz]
      p <- map (fmap fromIntegral) lattice
      e <- basis
      pure (Segment 0 p (p+e))
    refine s@(Segment n _ _) | n < cubics && n `mod` 2 == 0 = cubicToFccRefine s
                             | n < cubics && n `mod` 2 == 1 = fccToCubicRefine s
                             | n < cubics + bisects         = bisectRefine s
                             | otherwise                    = []

radialBinarySearch :: (Monad m, Applicative (Concurrently m))
                   => [V3 Rational]
                   -> Int
                   -> SegmentSearch m (V3 Rational)
radialBinarySearch directions bisections f =
  refineSearchList refine f initial
  where
    initial = [Segment 0 (V3 0 0 0) v | v <- directions]
    refine s@(Segment n _ _) | n < bisections = bisectRefine s
                             | otherwise   = []

tiledIcosahedron :: [V3 Rational]
tiledIcosahedron =
  concat [ [top]
         , zipWith mid row1 (repeat top)
         , row1
         , zipWith mid row1 (drop 1 (cycle row1))
         , zipWith mid row2 row1
         , zipWith mid row2 (drop 1 (cycle row1))
         , zipWith mid row2 (drop 1 (cycle row2))
         , row2
         , zipWith mid row2 (repeat bot)
         , [bot]
         ]
  where
    top = V3 0 0 1 :: V3 Rational
    bot = V3 0 0 (-1)
    upperAngles = [ V2 (305/341) 0
                  , V2 (233/322) (143/272), V2 (89/322) (131/154)
                  , V2 (-89/322) (131/154), V2 (-233/322) (143/272)
                  ]
    angles = upperAngles ++ map negate upperAngles
    row1 = [V3 x y (305/682)  | V2 x y <- evens angles]
    row2 = [V3 x y (-305/682) | V2 x y <- odds angles]
    mid u v = (u + v) ^/ 2
    evens []       = []
    evens (x : xs) = x : odds xs
    odds = evens . drop 1

fmapSearch :: Functor m => (a -> b) -> SegmentSearch m a -> SegmentSearch m b
fmapSearch f search g = fmap (fmap (fmap f)) (search (g . fmap f))

overRegion :: (Functor m, Additive v, Foldable v, Fractional a, Fractional (v Rational), Num (v a))
           => Region v a
           -> (LatticeSize v -> SegmentSearch m (v Rational))
           -> SegmentSearch m (v a)
overRegion Region{..} search = fmapSearch toRegion (search regionDensity)
  where
    d = fmap fromIntegral regionDensity
    toRegion p = regionOrigin + fmap fromRational (p/d) *! regionBasis

hex2dSearch :: (Monad m, Fractional a, Applicative (Concurrently m)) => Int -> Int -> Region V2 a -> SegmentSearch m (V2 a)
hex2dSearch hexs bisections r = overRegion r (hex2dLatticeSearch hexs bisections)

quad2dSearch :: (Monad m, Fractional a, Applicative (Concurrently m)) => Int -> Int -> Region V2 a -> SegmentSearch m (V2 a)
quad2dSearch quads bisections r = overRegion r (quad2dLatticeSearch quads bisections)

cubic3dSearch :: (Monad m, Fractional a, Applicative (Concurrently m)) => Int -> Int -> Region V3 a -> SegmentSearch m (V3 a)
cubic3dSearch cubics bisections r = overRegion r (cubic3dLatticeSearch cubics bisections)

-- Unlike hex2d, quad2d, and cubic3d searches, the star search
-- is centered at regionOrigin and extends approximately to +- each
-- basis element
starSearch :: (Monad m, Fractional a, Applicative (Concurrently m)) => [V3 Rational] -> Int -> Region V3 a -> SegmentSearch m (V3 a)
starSearch starPoints bisections r = overRegion r (const (radialBinarySearch starPoints bisections))

segments :: DB.KeyValMap (Segment a) ()
segments = DB.KeyValMap "segments"

-- We use a Set to automatically return False if a segment has been
-- seen before. This avoids testing a segment multiple times, but it
-- also ruins the tree structure of the search. For now, this isn't a
-- problem.
runSegmentSearch
  :: (Ord a, ToJSON a, DB.HasDB env, MonadReader env m, MonadIO m, MonadCatch m)
  => SegmentSearch m a
  -> (a -> m Bool)
  -> m [Segment a]
runSegmentSearch search testPoint = do
  f <- liftIO (memoize testPoint)
  m <- liftIO (newTVarIO Set.empty)
  search $ \s@(Segment n p q) -> do
    isNewSegment <- liftIO . atomically $ do
      b <- fmap (Set.notMember s) (readTVar m)
      when b (modifyTVar m (Set.insert s))
      return b
    if isNewSegment
      then do
        (x, y) <- liftA2 (,) (f p) (f q)
        when (x /= y) $ DB.insert segments (if x then s else Segment n q p) ()
        pure (x /= y)
      else
        pure False
