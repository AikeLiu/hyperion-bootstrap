{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}

module Hyperion.Bootstrap.SDPDeriv.BFGS where

import           Data.Aeson                         (FromJSON, ToJSON)
import           Data.Binary                        (Binary)
import           Data.Maybe                         (fromJust, fromMaybe)
import           GHC.Generics                       (Generic)
import           GHC.TypeNats                       (KnownNat)
import qualified Hyperion.Bootstrap.SDPDeriv.Jet2   as Jet2
import           Hyperion.Bootstrap.SDPDeriv.Newton (approxRational')
import           Hyperion.Static                    (Dict (..), Static (..),
                                                     cAp, cPtr)
import           Hyperion.Static.Orphans            ()
import           Linear.Metric                      (norm)
import           Linear.V                           (V)
import           Type.Reflection                    (Typeable)

import qualified Bootstrap.Math.Linear              as L
import           Data.Matrix.Static                 (Matrix)
import qualified Linear.Matrix                      as L
import qualified Linear.Metric                      as L
import           Linear.Vector                      ((*^), (^/))

data Config n = MkConfig
  { -- | Small deviation used when computing the gradient and hessian
    epsGradHess         :: Rational
    -- | Resolution used when converting numerical result for Newton step to Rational
  , stepResolution      :: Rational
    -- | Terminate the Newton search when the step size gets smaller than this amount.
  , gradNormThreshold   :: Rational
    -- | If true, the search terminates at the first negative point found
  , stopOnNegative      :: Bool
    -- | Minimum vertex of the bounding box
  , boundingBoxMin      :: V n Rational
    -- | Maximum vertex of the bounding box
  , boundingBoxMax      :: V n Rational
    -- | Hessian approximation to use when starting the search (will be
    -- |   calculated from bounding box if not given)
  , initialHessianGuess :: Maybe (Matrix n n Rational)
  } deriving (Eq, Ord, Show, Generic, Binary)

instance (KnownNat n) => Static (Binary (Config n)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp` closureDict @(KnownNat n)

defaultConfig :: V n Rational -> V n Rational -> Config n
defaultConfig bbMin bbMax = MkConfig
  { epsGradHess         = 1e-9
  , stepResolution      = 1e-32
  , gradNormThreshold   = 1e-9
  , stopOnNegative      = True
  , boundingBoxMin      = bbMin
  , boundingBoxMax      = bbMax
  , initialHessianGuess = Nothing
  }

data IslandExtConfig n = MkIslandExtConfig
  { -- | Params for underlying BFGS update
    bfgsConfig         :: Config n
    -- | Termination threshold for objective
  , goalToleranceObj   :: Rational
    -- | Termination threshold for perpendicular part of gradient
  , goalToleranceGrad  :: Rational
    -- | Factor to shrink the objective by in each iteration (need not be <1)
  , lineSearchDecrease :: Rational
    -- | Bound on number of line searches to do. If lineSearchDecrease >= 1,
    -- | convergence is not guaranteed, so this should be set
  , maxIters           :: Maybe Int
  } deriving (Eq, Ord, Show, Generic, Binary)

instance (KnownNat n) => Static (Binary (IslandExtConfig n)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp` closureDict @(KnownNat n)

defaultExtremaConfig :: Config n -> IslandExtConfig n
defaultExtremaConfig solverConfig = MkIslandExtConfig
  { bfgsConfig         = solverConfig
  , goalToleranceObj   = 1e-6
  , goalToleranceGrad  = 1e-3
  , lineSearchDecrease = 0.8
  , maxIters           = Nothing
  }

data BFGSSearchStatus = Success | BoundaryExceeded | EvaluationError
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

data BFGSData n a = MkBFGSData
  { x            :: V n Rational
  , constant     :: a
  , gradient     :: V n a
  , hessianGuess :: Maybe (Matrix n n a)
  , searchStatus :: BFGSSearchStatus
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance (KnownNat n, Typeable a, Static (Binary a)) => Static (Binary (BFGSData n a)) where
  closureDict = cPtr (static (\Dict Dict -> Dict)) `cAp`
                closureDict @(KnownNat n) `cAp`
                closureDict @(Binary a)

-- | Given a monadic function `getValueAndGradient` that takes a precision
-- and a point and returns a BFGSData object containing the value of
-- the objective and its gradient (and optionally its Hessian), performs
-- a BFGS descent starting from 'xInitial'
run
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => Config n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> V n Rational
  -> m (V n Rational, [BFGSData n a]) -- TODO: change tuple to data?
run cfg@MkConfig{..} getValueAndGradient xInitial =
  do
    initialPointData <- getValueAndGradient epsGradHess xInitial
    allPoints <- go [initialPointData]
    pure (x (head allPoints), allPoints)
  where
    -- Flow control and stop condition/bounds checking
    -- (keeping the whole list of points is probably not needed, but could make
    --  it easy to troubleshoot in the future)
    go pointList@(MkBFGSData{..}:_)
      | stopOnNegative && constant < 0                 = pure pointList
      | norm gradient < fromRational gradNormThreshold = pure pointList
      | searchStatus == BoundaryExceeded               = pure pointList
      | otherwise = do
          nextPoint <- bfgsStep cfg getValueAndGradient (head pointList)
          go (nextPoint : pointList)
    -- quash warning (should never call this with empty list of points)
    go [] = pure []

-- | Given a monadic function `getValueAndGradient`, the BFGS output
-- for an initial point, and a direction to extremize in, finds the
-- tip of the region where the objective is negative.
-- Based on Algorithm 2 in arXiv:2104.09518
extremizeIsland
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => IslandExtConfig n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> m (V n Rational, [BFGSData n a])
extremizeIsland MkIslandExtConfig{..} getValueAndGradient initialPointData direction =
  go 1 [initialPointData] initialPointData
  where
    -- testing of end condition
    endCondition constant gradient = let
      gtol = fromRational goalToleranceObj
      gtolD = fromRational goalToleranceGrad
      projected = L.project (fromRational <$> direction) gradient
      in (abs constant <= gtol) && (norm (gradient - projected) <= gtolD)
    -- Main flow control
    -- lastBFGS is the point at which the hessian was most recently updated
    go iter pointList@(point:_) lastBFGS
      | fmap (<iter) maxIters == Just True             = pure (x point, pointList)
      | endCondition (constant point) (gradient point) = pure (x point, pointList)
      | otherwise = do
          let stepRes = stepResolution bfgsConfig
              xExtremum = extremizeEllipsoid lastBFGS (fromRational <$> direction)
              stepDirection = fmap (approxRational' stepRes) xExtremum
                              - (x point)
          pointNew <- interpolatingLineSearch
                      (getValueAndGradient (epsGradHess bfgsConfig))
                      point stepDirection stepRes lineSearchDecrease
          -- hard to explain, see last conditional in paper loop
          let dx = fromRational <$> (x pointNew - x lastBFGS)
              dgrad = (gradient pointNew - gradient lastBFGS)
              curvatureCondition = L.dot dx dgrad > 0
              pointNew' = pointNew { hessianGuess =
                                       if curvatureCondition
                                       then updatedHessian lastBFGS pointNew
                                       else hessianGuess point
                                   }
              lastBFGS' = if curvatureCondition then pointNew' else lastBFGS
          -- done, might've updated hessian
          go (iter+1) (pointNew' : pointList) lastBFGS'
    -- quash warning (should never call this with empty list of points)
    go _ [] _ = go 1 [initialPointData] initialPointData

-- | Core updating math of BFGS
bfgsStep
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => Config n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> m (BFGSData n a)
bfgsStep MkConfig{..} getValueAndGradient point@MkBFGSData{..} =
  do
    pointNew <- approxLineSearch (getValueAndGradient epsGradHess)
                point' stepDirection' aMax
    -- line search might not provide hessian, need to get it post-hoc
    pure $ pointNew{ hessianGuess = updatedHessian point' pointNew }
  where
    boxDims = fromRational <$> (boundingBoxMax - boundingBoxMin)
    -- if user doesn't provide a default hessian, use bounding box
    hessianScale = norm gradient / 0.2 -- from Navigator paper alg. 1
    fallbackHessian = case initialHessianGuess of
      Just b0 -> fromRational <$> b0
      Nothing -> L.diagonalMatrix $ (hessianScale /) <$> boxDims
    -- incorporate hessian guess into point for line search
    b = fromMaybe fallbackHessian hessianGuess
    point' = point{ hessianGuess = Just b }
    -- different types, so do rational approx of step direction
    stepDirection  = Jet2.newtonStep (Jet2.MkJet2 constant gradient b)
    stepDirection' = fmap (approxRational' stepResolution) stepDirection
    -- our upper bound on alpha is the distance to next box wall
    upperWallDists = (boundingBoxMax - x) / stepDirection'
    lowerWallDists = (boundingBoxMin - x) / stepDirection'
    aMax = foldr1 min (max <$> upperWallDists <*> lowerWallDists)

-- | Given two points, maybe return an approximation to the Hessian of the
-- second based on the Hessian of the first
-- (returns Nothing if hessianGuess point1 == Nothing)
-- (returns Just b if hessianGuess point2 == Just b)
updatedHessian
  :: (KnownNat n, RealFrac a)
  => BFGSData n a
  -> BFGSData n a
  -> Maybe (Matrix n n a)
updatedHessian point1 point2 =
  case hessianGuess point2 of
    Just bfinal -> Just bfinal
    Nothing -> do
      b0 <- (hessianGuess point1) -- fail if point1 is Nothing
      -- step size and change in gradient (same notation as Wikipedia):
      let s = fromRational <$> (x point2 - x point1)
          y = gradient point2 - gradient point1
          b0s = b0 L..* s
          bDiff = (L.outer y y) ^/ (L.dot s y)
                  - (L.outer b0s b0s) ^/ (L.dot s b0s)
      pure (b0 + bDiff)

-- | Given a second-order approximation to a function N(x), find the extremum
-- (in the given direction) of the ellipsoid where N(x) = 0
extremizeEllipsoid
  :: (KnownNat n, RealFrac a, Floating a)
  => BFGSData n a
  -> V n a
  -> V n a
extremizeEllipsoid MkBFGSData{..} direction =
  -- (-lambda) will give another extremum, but this direction because we
  -- assume the hessian is positive-definite
  x' - (applyInverseB gradient) + (applyInverseB direction) ^/ lambda
  where
    x' = fromRational <$> x
    hessian = fromJust hessianGuess -- we never call this without a Hessian
    applyInverseB = L.luSolve (L.toRows hessian)
    lambda = sqrt ( (direction `L.dot` (applyInverseB direction))
                    / (gradient `L.dot` (applyInverseB gradient) - 2*constant)
                  )

-- | Monadic implementation of an approximate line search satisfying the Wolfe
-- conditions, from Wright & Nocedal p. 60
-- (I think it's the same as More-Thuente, but the M-T paper is unreadable)
-- Returns grad information too, so we don't have to redo a job
approxLineSearch
  :: (KnownNat n, RealFrac a, Monad m)
  => (V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> Rational
  -> m (BFGSData n a)
approxLineSearch getBFGSData initialPoint direction alphaMax =
  if alphaMax > 0
  then go (1::Int) alpha0 0.0 (constant initialPoint)
  else pure (initialPoint {searchStatus = BoundaryExceeded})

  where
    mu    = 0.0001 -- for sufficient decrease condution
    eta   = 0.9    -- for curvature condition
    delta = 1.5    -- factor to multiply alpha by when we extend our region

    -- setup for solver call
    alpha0 = if alphaMax > 1 then 1 else alphaMax/2
    (f0, df0) = bfgsDataToJet1 initialPoint

    go iter alpha alphaPrev fPrev = do
      pointData <- getBFGSData ((x initialPoint) + alpha *^ direction)
      let (f, df) = bfgsDataToJet1 pointData
      if | alpha > alphaMax         -> pure (pointData {searchStatus = BoundaryExceeded})
         | (f > f0 + df0*mu*(fromRational alpha)) || (iter > 1 && f >= fPrev)
                                    -> zoom alphaPrev alpha fPrev
         | abs df <= -eta * df0     -> pure pointData
         | df > 0                   -> zoom alpha alphaPrev f
         | otherwise                -> go (iter+1) (delta * alpha) alpha f

    zoom low high fLow = do -- (low < high) is not necessarily true
      let alpha = interpolate low high
      pointData <- getBFGSData ((x initialPoint) + alpha *^ direction)
      let (f, df) = bfgsDataToJet1 pointData
      if | (f > f0 + df0*mu*(fromRational alpha)) || (f >= fLow)
                                                   -> zoom low alpha fLow
         | abs df <= -eta * df0                    -> pure pointData
         | df * (fromRational (high - low)) >= 0   -> zoom alpha low f
         | otherwise                               -> zoom alpha high f

    bfgsDataToJet1 dat = (constant dat,
                          L.dot (gradient dat) (fromRational <$> direction))

    interpolate low high = (low + high) / 2.0 -- could use other methods

interpolatingLineSearch
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => (V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> Rational
  -> Rational
  -> m (BFGSData n a)
interpolatingLineSearch getBFGSData initialPoint direction
  stepResolution decreaseThreshold = go alpha0
  where
    navTarget = (fromRational decreaseThreshold) * abs (constant initialPoint)
    alpha0 = 1.0

    go alpha = do
      alphaPoint <- getBFGSData ((x initialPoint) + alpha *^ direction)
      let (f0, df0) = bfgsDataToJet1 initialPoint
          (f1, df1) = bfgsDataToJet1 alphaPoint
          interpolator = cubicInterpolatorUnit f0 df0 f1 df1
      if | (constant alphaPoint) <= navTarget -> pure alphaPoint
         | constant initialPoint < 0
           -> go $ alpha * approxRational' stepResolution (solveCubic' interpolator)
         | otherwise
           -> go $ alpha * approxRational' stepResolution (extremizeCubic' interpolator)

    -- gets all roots, returns the one closest to 0.5 (middle of test interval)
    solveCubic' cubic = foldr1 selector (solveCubic cubic)
      where selector x y = if abs (x-0.5) < abs (y-0.5) then x else y

    -- gets minimum if it exists, otherwise return default of 0.5
    extremizeCubic' cubic = case (extremizeCubic cubic) of
                              []      -> 0.5
                              extrema -> foldr1 selector extrema
      where selector = if (c3 cubic > 0) then max else min

    bfgsDataToJet1 dat = (constant dat,
                          L.dot (gradient dat) (fromRational <$> direction))

data CubicPolynomial a = MkCubic
  { c0 :: a,
    c1 :: a,
    c2 :: a,
    c3 :: a
  } deriving (Eq, Ord, Show)

-- | Evaluates a cubic polynomial at the given value of x
evalCubic
  :: (RealFrac a)
  => CubicPolynomial a -> a -> a
evalCubic MkCubic{..} x = c0 + c1*x + c2*x*x + c3*x*x*x

-- | Constructs a cubic interpolating polynomial on the unit interval
-- given the values and derivatives of a function at 0 and 1
cubicInterpolatorUnit
  :: (RealFrac a)
  => a -> a -> a -> a -> CubicPolynomial a
cubicInterpolatorUnit f0 df0 f1 df1 = MkCubic{..}
  where -- from gaussian elimination:
    c0 = f0
    c1 = df0
    c2 = 3*(f1 - c0 - c1) - (df1 - c1)
    c3 = f1 - c0 - c1 - c2

-- | Given a cubic equation, returns a list of all real local extrema
extremizeCubic
  :: (RealFrac a, Floating a)
  => CubicPolynomial a -> [a]
extremizeCubic MkCubic{..} = realQuadraticRoots c1 (2*c2) (3*c3)

-- | Given a cubic, returns a list of all real roots
--   roots with multiplicity >1 are returned multiple times
solveCubic
  :: (RealFrac a, Floating a)
  => CubicPolynomial a -> [a]
solveCubic MkCubic{..}
  | c3 == 0            = realQuadraticRoots c0 c1 c2 -- just a quadratic
  | p == 0 && q == 0   = unDepress <$> [0, 0, 0]     -- triple root
  | p == 0             = unDepress <$> [cubeRoot (-q)] -- single root, no x term
  | discriminant <  0  = unDepress <$> depressedSingleRoot -- general single root
  | otherwise          = unDepress <$> depressedMultiRoots -- general 2-3 roots
  where
    discriminant = 18*c3*c2*c1*c0 - 4*c2*c2*c2*c0 + c2*c2*c1*c1
                   -4*c3*c1*c1*c1 - 27*c3*c3*c0*c0 -- to get number of roots

    -- do Cardano reduction and solve depressed cubic with trig:
    depressedMultiRoots = [ 2 * sqrt(-p/3)
                            * cos( acos(trigClamp tmp)/3 - 2*pi*k/3 )
                          | k <- [0,1,2] ]
    depressedSingleRoot = if p >= 0 then
                            [ -2 * sqrt(p/3) * sinh((asinh tmp) / 3) ]
                          else
                            [ -2 * (signum q) * sqrt(-p/3)
                              * cosh((acosh (abs tmp)) / 3) ]
    tmp = sqrt(3/(abs p)) * (3*q) / (2*p)

    -- definition of depressed cubic:
    p = (3*c3*c1 - c2*c2) / (3*c3*c3)
    q = (2*c2*c2*c2 - 9*c3*c2*c1 + 27*c3*c3*c0) / (27*c3*c3*c3)
    unDepress t = t - c2 / (3 * c3)

    -- fix overflow before calling asin or acos
    trigClamp x | x >  1    =  1
                | x < -1    = -1
                | otherwise = x

-- | utility functions for intermediate steps in cubic polynomials
realQuadraticRoots :: (RealFrac a, Floating a) => a -> a -> a -> [a]
realQuadraticRoots c0 c1 c2
  | c2 == 0            = [-c0 / c1] -- no x^2 term; just a line
  | discriminant  < 0  = []
  | discriminant == 0  = [-c1 / (2*c2)]
  | otherwise          = [ (-c1 - sqrt discriminant) / (2*c2)
                         , (-c1 + sqrt discriminant) / (2*c2)
                         ]
  where
    discriminant = c1*c1 - 4*c0*c2

cubeRoot :: (RealFrac a, Floating a) => a -> a
cubeRoot x | x < 0     = -cubeRoot (-x)
           | otherwise = x**(1/3)
