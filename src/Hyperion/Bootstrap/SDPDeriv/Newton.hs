{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StaticPointers    #-}

module Hyperion.Bootstrap.SDPDeriv.Newton where

import           Data.Binary                      (Binary)
import           Data.Ratio                       (approxRational)
import           GHC.Generics                     (Generic)
import           GHC.TypeNats                     (KnownNat)
import qualified Hyperion.Bootstrap.SDPDeriv.Jet2 as Jet2
import           Hyperion.Static                  (Dict (..), Static (..), cPtr)
import           Hyperion.Static.Orphans          ()
import           Linear.Metric                    (norm)
import           Linear.V                         (V)

data Config = MkConfig
  { -- | Small deviation used when computing the gradient and hessian
    epsGradHess       :: Rational
    -- | Resolution used when converting numerical result for Newton step to Rational
  , stepResolution    :: Rational
    -- | Terminate the Newton search when the step size gets smaller than this amount.
  , stepNormThreshold :: Rational
  } deriving (Eq, Ord, Show, Generic, Binary)

instance Static (Binary Config) where closureDict = cPtr (static Dict)

-- | A flipped version of approxRational that takes the resolution as
-- a Rational.
approxRational' :: RealFrac a => Rational -> a -> Rational
approxRational' eps x = x `approxRational` fromRational eps

-- | Given a monadic function getJet that takes the current number of
-- Newton steps and a point and returns a 'Jet2', perform a search
-- starting from 'xInitial' using Newton's method.
run
  :: (KnownNat n, RealFrac a, Floating a , Monad m)
  => Config
  -> (Rational -> V n Rational -> m (Jet2.Jet2 n a))
  -> V n Rational
  -> m (V n Rational, [Jet2.Jet2 n a])
run cfg getJet xInitial =
  go xInitial []
  where
    go x jets = do
      j <- getJet (epsGradHess cfg) x
      let
        step = Jet2.newtonStep j
        stepApprox = fmap (approxRational' (stepResolution cfg)) step
      if norm step < fromRational (stepNormThreshold cfg)
        then pure (x, reverse (j : jets))
        else go (x + stepApprox) (j : jets)
