{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Hyperion.Bootstrap.OPESearch.FormQueries where

import           Control.Applicative                           ((<|>))
import           Control.Monad.Catch                           (catch)
import           Control.Monad.Except                          (runExceptT)
import           Control.Monad.Trans.Maybe                     (MaybeT (..),
                                                                runMaybeT)
import           Data.List.NonEmpty                            (NonEmpty (..),
                                                                (<|))
import qualified Data.List.NonEmpty                            as NE
import qualified Data.Matrix.Static                            as M
import           Data.Proxy                                    (Proxy)
import           Data.Ratio                                    (approxRational)
import           Fmt                                           ((+||), (||+))
import           GHC.TypeNats
import qualified Hyperion.Log                                  as Log
import qualified Bootstrap.Math.BoolFunction                as B
import           Hyperion.Bootstrap.OPESearch.BilinearForms     (BilinearForms (..),
                                                                midMaxPositiveInterval,
                                                                quadraticPositive)
import           Hyperion.Bootstrap.OPESearch.HessianLineSearch (DescentConfig (..),
                                                                DescentResult (..),
                                                                HessianLineSearchError (..),
                                                                LineSearchType (..),
                                                                descentSearchRandomized,
                                                                hessianLineSearchAverage)
import           Linear.V                                      (V)
import           Numeric.Rounded                               (Rounded, RoundingMode (..),
                                                                reifyPrecision)
import qualified QuadraticNet                                  as QN

queryAllowedBoolFunction :: Monad m => BilinearForms 2 -> m (Maybe (V 2 Rational))
queryAllowedBoolFunction (BilinearForms res fs) = pure $
  let allowed = B.getAll $ foldMap (B.MkAll . quadraticPositive . fmap fromRational . snd) fs
  in fmap (fmap (`approxRational` res)) $ midMaxPositiveInterval allowed

queryAllowedQuadraticNet
  :: forall j . (KnownNat j, KnownNat (j-1))
  => QN.QuadraticNetConfig
  -> Double
  -> BilinearForms j
  -> IO (Maybe (V j Rational))
queryAllowedQuadraticNet qNetConfig resolution (BilinearForms _ fs) =
  reifyPrecision (QN.precision qNetConfig) $
    \(_ :: Proxy p) -> do
      -- We negate the matrices because we want to find a region where
      -- all quadratic forms are negative. We rescale so that various
      -- thresholds defined in QuadraticNet work correctly.
      let qForms :: NE.NonEmpty (M.Matrix j j (Rounded 'TowardZero p)) =
            rescaleNegateMatrix <$> NE.fromList (map snd fs)
      vMaybe <-
        -- withKnownNat (SNat @j %:- SNat @1) $
        noteError (QN.runSearch qNetConfig qForms)
      return $ fmap (fmap (`approxRational` realToFrac resolution)) vMaybe
  where
    noteError m = runExceptT m >>= \case
      Left e -> Log.info "QuadraticNet error" e >> return Nothing
      Right r -> return r

rescaleMatrix :: (KnownNat j, Fractional a) => M.Matrix j j a -> M.Matrix j j a
rescaleMatrix m = fmap (/ scale) m
  where
    scale = sum (map abs (M.toList m)) / fromIntegral (M.ncols m * M.nrows m)

rescaleNegateMatrix :: (KnownNat j, Fractional a) => M.Matrix j j Rational -> M.Matrix j j a
rescaleNegateMatrix = rescaleMatrix . negate . fmap realToFrac

queryAllowedDescent
  :: forall j . (KnownNat j, 1 <= j)
  => DescentConfig
  -> Int
  -> Double
  -> Int
  -> Int
  -> BilinearForms j
  -> IO (Maybe (V j Rational))
queryAllowedDescent descentConfig prec resolution hessianLineSteps hessianLineAverage (BilinearForms _ qs) =
  reifyPrecision prec $
  \(_ :: Proxy p) -> case qs of
    (_, qNewR) : (Just lambdaPrevR, qForm'R) : qFormsR -> do
      let lambdaPrev :: V j (Rounded 'TowardZero p) = fmap realToFrac lambdaPrevR
          qNew = rescaleNegateMatrix qNewR
          qForms = rescaleNegateMatrix <$> qForm'R :| map snd qFormsR
      Log.text $
        "Starting descent search with "+||length qForms + 1||+
        " quadratic forms of size "+||M.nrows qNew||+
        "x"+||M.ncols qNew||+""
      descentSearchRandomized descentConfig qForms qNew lambdaPrev >>= \case
        Feasible n tilt lambda -> do
          Log.info "Descent search succeeded" (n, tilt, lambda)
          lambda' <- hessianLineSearchAverage MidPoint (qNew <| qForms) lambda hessianLineSteps hessianLineAverage
          return (Just (fmap (`approxRational` realToFrac resolution) lambda'))
        Infeasible -> do
          Log.text "Descent search failed."
          return Nothing
    _ -> return Nothing

data QueryMixedConfig = QueryMixedConfig
  { qmDescentConfig      :: DescentConfig
  , qmQuadraticNetConfig :: QN.QuadraticNetConfig
  , qmPrecision          :: Int
  , qmResolution         :: Double
  , qmHessianLineSteps   :: Int
  , qmHessianLineAverage :: Int
  }

queryAllowedMixed
  :: (KnownNat j, KnownNat (j-1), 1 <= j)
  => QueryMixedConfig
  -> BilinearForms j
  -> IO (Maybe (V j Rational))
queryAllowedMixed QueryMixedConfig{..} b =
  runMaybeT (MaybeT tryDescent <|> MaybeT tryQN)
  where
    tryDescent =
      queryAllowedDescent qmDescentConfig qmPrecision qmResolution qmHessianLineSteps qmHessianLineAverage b
      `catch` (\(e :: HessianLineSearchError) -> Log.info "HessianLineSearchError" e >> return Nothing)
    tryQN = queryAllowedQuadraticNet qmQuadraticNetConfig qmResolution b

