{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Hyperion.Bootstrap.OPESearch.BilinearForms where

import           Bootstrap.Math.BoolFunction (BoolFunction)
import qualified Bootstrap.Math.BoolFunction as B
import qualified Bootstrap.Math.Linear       as L
import           Data.Aeson                  (FromJSON, ToJSON)
import           Data.Binary                 (Binary)
import           Data.List.Extra             (maximumOn)
import           Data.Matrix.Static          (Matrix)
import           Data.Ratio                  (approxRational)
import           Data.Vector.Binary          ()
import           GHC.Generics                (Generic)
import           Linear.V                    (V)

data BilinearForms j = BilinearForms
  { bfResolution :: Double
  , bfMatrices   :: [(Maybe (V j Rational), Matrix j j Rational)]
  } deriving (Show, Eq, Generic, Binary, FromJSON, ToJSON)

addBilinearForm :: Matrix j j Rational -> BilinearForms j -> BilinearForms j
addBilinearForm m (BilinearForms r ms) = BilinearForms r ((Nothing, m) : ms)

setFeasibleVector :: Maybe (V j Rational) -> BilinearForms j -> BilinearForms j
setFeasibleVector mv b = case b of
  BilinearForms r ((_, m) : ms) -> BilinearForms r ((mv, m) : ms)
  _ -> error "Cannot set feasible vector for empty bilinear forms"

feasibleVector :: BilinearForms j -> Maybe (V j Rational)
feasibleVector (BilinearForms _ ((mv,_) : _)) = mv
feasibleVector _                              = Nothing

thetaIntervalFormApprox :: Double -> Rational -> Rational -> Matrix 2 2 Rational
thetaIntervalFormApprox res thMin' thMax' =
  fmap (`approxRational` res) (thetaIntervalForm (fromRational thMin') (fromRational thMax'))
  where
    thetaIntervalForm thMin thMax = L.toM
      ( ( 2 * sin thMin * sin thMax, - sin (thMin + thMax) )
      , ( - sin (thMin + thMax)    , 2 * cos thMin * cos thMax )
      )

thetaVectorApprox :: Double -> Rational -> V 2 Rational
thetaVectorApprox res th' =
  fmap (`approxRational` res) (thetaVector (fromRational th'))
  where
    thetaVector th = L.toV (cos th, sin th)

-- | A 2x2 bilinear form ((a,b),(b,d)) defines a univariate quadratic
-- polynomial p(x) = a + 2*b*x + d*x^2 via its pairing with the vector
-- (1,x). This function returns a 'BoolFunction' given by x -> p(x) >
-- 0.
quadraticPositive :: (Floating a, Ord a) => Matrix 2 2 a -> BoolFunction a
quadraticPositive (L.M22 a b _ d) =
  if
     | d == 0 && b == 0 -> B.constant $ a > 0
     | d == 0 && b > 0  -> B.step $ -a/(2*b)
     | d == 0 && b < 0  -> B.not $ B.step $ -a/(2*b)
     | disc <= 0        -> B.constant $ a > 0
     | d < 0            -> B.interval t2 t1
     | otherwise        -> B.not $ B.interval t1 t2
  where
    disc = b*b - a*d
    t1 = (-b - sqrt disc)/d
    t2 = (-b + sqrt disc)/d

midMaxPositiveInterval :: (Floating a, Ord a) => BoolFunction a -> Maybe (V 2 a)
midMaxPositiveInterval f = case B.trueIntervals f of
    []            -> Nothing
    trueIntervals -> Just $ midpoint $ maximumOn vecAngle trueIntervals
  where
    vecAngle (l,u) = acos (B.toHalfCircle l L..* B.toHalfCircle u)
    midpoint (B.NegInfinity, B.PosInfinity) = B.toHalfCircle (B.Finite 0)
    midpoint (l,u) = L.normalize (B.toHalfCircle l + B.toHalfCircle u)

