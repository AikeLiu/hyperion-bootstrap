{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE TypeApplications  #-}

module Hyperion.Bootstrap.Tiptop where

import           Data.Aeson           (FromJSON (..), ToJSON (..))
import qualified Data.Aeson           as A
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Foldable        as Foldable
import qualified Data.Scientific      as S
import qualified Data.Vector          as V
import           GHC.Generics         (Generic)
import           GHC.IO.Handle        (hClose)
import qualified Hyperion.Log         as Log
import           Linear.V             (Dim, V)
import qualified Linear.V             as L
import           System.Process       (CreateProcess (..), StdStream (..), proc,
                                       withCreateProcess)

data TTV n a = TTV (V n a) Int
  deriving (Show, Functor)

instance Dim n => ToJSON (TTV n Double) where
  toJSON (TTV v i) = toJSON $ Foldable.toList v ++ [fromIntegral i]

data Input n = Input
  { feasible    :: V.Vector (TTV n Double)
  , infeasible  :: V.Vector (TTV n Double)
  , in_progress :: V.Vector (TTV n Double)
  , max_gap     :: Int
  , resolution  :: Double
  } deriving (Generic, ToJSON)

data Output n = NextPoint (TTV n Double) | NoPoint
  deriving Show

instance Dim n => FromJSON (Output n) where
  parseJSON val = do
    sciVec <- A.parseJSON val
    case V.length sciVec of
      0 -> pure NoPoint
      _ -> do
        v <- fmap S.toRealFloat <$> case L.fromVector (V.init sciVec) of
          Just v' -> pure v'
          _       -> fail "Found vector of unexpected length"
        i <- case S.floatingOrInteger @Double @Int (V.last sciVec) of
          Right i' -> pure i'
          _        -> fail "Expected an integer but found a double"
        pure (NextPoint (TTV v i))

tiptop :: FilePath -> Input 3 -> IO (Output 3)
tiptop tiptopExecutable input =
  withCreateProcess tiptopProc $ \stdin stdout _ _ ->
    case (stdin, stdout) of
      (Just hin, Just hout) -> do
        let inString = A.encode input
        Log.info "toptop input" inString
        Log.info "feasible points" (V.length (feasible input))
        Log.info "infeasible points" (V.length (infeasible input))
        Log.info "in_progress points" (V.length (in_progress input))
        Log.info "total points" $ sum $ map (\f -> V.length (f input)) [feasible, infeasible, in_progress]
        LBS.hPut hin (A.encode input)
        hClose hin
        out <- LBS.hGetContents hout
        Log.info "tiptop output" out
        case A.eitherDecode out of
          Right r -> return r
          Left e  -> Log.throwError $ "Could not parse tiptop output: " ++ e ++ show out
      _ -> Log.throwError "Couldn't create pipe to tiptop process"
  where
    tiptopProc = (proc tiptopExecutable ["-", "-"])
      { std_in  = CreatePipe
      , std_out = CreatePipe
      }

